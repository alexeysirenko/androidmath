package com.drunkpacifist.androidcalculator.expression;

import com.drunkpacifist.androidcalculator.engine.tokens.Token;
import com.drunkpacifist.androidcalculator.engine.tokens.numbers.NumericToken;
import com.drunkpacifist.androidcalculator.engine.tokens.operators.math.MathOperatorToken;
import com.drunkpacifist.androidcalculator.engine.tokens.operators.parentheses.ParenthesesOperatorToken;
import com.drunkpacifist.androidcalculator.expression.commands.SimpleExpressionCommand;

import java.util.*;

/**
 * Builds expression using provided commands, returns list on tokens in infix form.
 */
public class ExpressionBuilder {

    /**
     * Holds info about decimal separator.
     */
    protected boolean lastDot = false;

    /**
     * List to store collected tokens.
     */
    private final LinkedList<Token> tokens = new LinkedList<>();

    /**
     * Adds token to expression.
     *
     * @param token token to add, not null
     */
    public void add(Token token) {
        tokens.add(token);
    }

    /**
     * Evaluates command and adds corresponding token to expression.
     *
     * @param command expression command, not null
     */
    public void add(SimpleExpressionCommand command) {
        if (command == SimpleExpressionCommand.DOT) {
            addCommandDot();
        } else {
            if (isCommandNumeric(command)) {
                addCommandNumeric(command);
            } else if (isCommandOperator(command)) {
                addCommandOperator(command);
            } else if (isCommandParenthesis(command)) {
                addCommandParenthesis();
            }
            lastDot = false;
        }
    }

    /**
     * Removes last element of expression. In case of number will remove last digit of one.
     *
     * @throws NoSuchElementException if this expression is empty.
     */
    public void removeLast() {
        lastDot = false;
        Token lastToken = tokens.pollLast();
        if (lastToken == null) {
            throw new NoSuchElementException("Expression has no elements");
        }
        if (isTokenNumeric(lastToken)) {
            String number = lastToken.toString();
            number = number.substring(0, number.length() - 1);
            if ((!number.isEmpty()) && (number.charAt(number.length() - 1) == '.')) {
                number = number.substring(0, number.length() - 1);
                lastDot = true;
            }
            if (!number.isEmpty()) {
                tokens.add(NumericToken.valueOf(number));
            }
        }
    }

    /**
     * Clears expression.
     */
    public void clear() {
        tokens.clear();
        lastDot = false;
    }

    /**
     * Returns number of tokens in expression.
     *
     * @return number of tokens in expression
     */
    public int size() {
        return tokens.size();
    }

    /**
     * Returns string representation of object.
     *
     * @return string representation of object.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Token token: tokens) {
            result.append(token);
        }
        return (result.length() > 0 ? result.toString() : "0") + (lastDot ? "." : "");
    }

    /**
     * Returns expression presented as list of tokens.
     *
     * @return list of tokens
     */
    public List<Token> toTokens() {
        return new ArrayList<>(tokens);
    }

    /**
     * Checks is token a numeric value (operand).
     *
     * @param token token to check, not null
     * @return is token a numeric value
     */
    private boolean isTokenNumeric(Token token) {
        return token instanceof NumericToken;
    }

    /**
     * Checks is token a mathematical operator.
     *
     * @param token token to check, not null
     * @return is token a mathematical operator
     */
    private boolean isTokenOperator(Token token) {
        return token instanceof MathOperatorToken;
    }

    /**
     * Checks is token a parenthesis.
     *
     * @param token token to check, not null
     * @return is token a parenthesis
     */
    private boolean isTokenParenthesis(Token token) {
        return token instanceof ParenthesesOperatorToken;
    }

    /**
     * Checks is command represents numeric value.
     *
     * @param command token to check, not null
     * @return is command a numeric value
     */
    private boolean isCommandNumeric(SimpleExpressionCommand command) {
        return EnumSet.range(SimpleExpressionCommand.ZERO, SimpleExpressionCommand.NINE).contains(command);
    }

    /**
     * Checks is command represents math operator.
     *
     * @param command token to check, not null
     * @return is command a math operator
     */
    private boolean isCommandOperator(SimpleExpressionCommand command) {
        return EnumSet.of(SimpleExpressionCommand.ADD, SimpleExpressionCommand.SUBSTRACT,
                SimpleExpressionCommand.MULTIPLY, SimpleExpressionCommand.DIVIDE).contains(command);
    }

    /**
     * Checks is command represents parentheses.
     *
     * @param command token to check, not null
     * @return is command parentheses
     */
    private boolean isCommandParenthesis(SimpleExpressionCommand command) {
        return command == SimpleExpressionCommand.PARENTHESIS;
    }

    /**
     * Adds decimal separator to expression.
     */
    private void addCommandDot() {
        if (!isTokenNumeric(tokens.peekLast())) {
            tokens.add(NumericToken.ZERO);
        }
        lastDot = true;
    }

    /**
     * Evaluates numeric command  and adds correct token to expression.
     *
     * @param command numeric command
     */
    private void addCommandNumeric(SimpleExpressionCommand command) {
        Token lastToken = tokens.peekLast();
        if (isTokenNumeric(lastToken)) {
            tokens.removeLast();
            String number = lastToken.toString() +
                    (lastDot ? "." : "") +
                    command.toString();
            lastDot = false;
            tokens.add(NumericToken.valueOf(number));
        } else {
            tokens.add(NumericToken.valueOf((lastDot ? "0." : "") + command.toString()));
        }
    }

    /**
     * Evaluates math operator command and adds correct token to expression.
     *
     * @param command math operator command
     */
    private void addCommandOperator(SimpleExpressionCommand command) {
        if (tokens.isEmpty() || isTokenParenthesis(tokens.peekLast())) {
            if (command == SimpleExpressionCommand.SUBSTRACT) {
                tokens.add(MathOperatorToken.UNARYMINUS);
            }
        } else {
            if (isTokenOperator(tokens.peekLast())) {
                tokens.removeLast();
            }
            tokens.add(MathOperatorToken.valueOf(command.toString()));
        }
    }

    /**
     * Adds correct parentheses token to expression.
     */
    private void addCommandParenthesis() {
        // TODO: improve parentheses choise logic
//        if (isTokenNumeric(tokens.peekLast())) {
//            tokens.add(MathOperatorToken.MULTIPLY);
//        }
        boolean isOpening = !(isTokenNumeric(tokens.peekLast()));
        tokens.add(isOpening ? ParenthesesOperatorToken.OPEN : ParenthesesOperatorToken.CLOSE);
    }
}
