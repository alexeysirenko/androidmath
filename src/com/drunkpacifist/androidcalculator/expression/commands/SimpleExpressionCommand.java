package com.drunkpacifist.androidcalculator.expression.commands;

/**
 * Created by Oleksiy on 20.09.2015.
 */
public enum SimpleExpressionCommand {
    ZERO("0"),
    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    DOT("."),
    ADD("+"),
    SUBSTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/"),
    PARENTHESIS("()")
    ;

    private final String value;

    SimpleExpressionCommand(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
