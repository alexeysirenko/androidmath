package com.drunkpacifist.androidcalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.drunkpacifist.androidcalculator.engine.exceptions.EvaluateException;
import com.drunkpacifist.androidcalculator.engine.exceptions.PostfixConvertException;
import com.drunkpacifist.androidcalculator.engine.facade.CalculatorFacade;
import com.drunkpacifist.androidcalculator.engine.tokens.Token;
import com.drunkpacifist.androidcalculator.engine.tokens.numbers.NumericToken;
import com.drunkpacifist.androidcalculator.expression.ExpressionBuilder;
import com.drunkpacifist.androidcalculator.expression.commands.SimpleExpressionCommand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculatorActivity extends Activity {

    private final Map<Integer, SimpleExpressionCommand> btnCommands = new HashMap<>();

    // TextView used to display the output
    private TextView txtScreen;

    private final ExpressionBuilder expressionBuilder = new ExpressionBuilder();

    private final CalculatorFacade calculatorFacade = new CalculatorFacade();


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        fillCommandTyples(btnCommands);
        this.txtScreen = (TextView) findViewById(R.id.txtScreen);
        txtScreen.setText(expressionBuilder.toString());
    }

    public void onCalculate(View view) {
        try {
            List<Token> expression = expressionBuilder.toTokens();
            expressionBuilder.clear();
            double result = calculatorFacade.processExpression(expression);
            expressionBuilder.add(NumericToken.valueOf(result));
            txtScreen.setText(expressionBuilder.toString());
        } catch (PostfixConvertException | EvaluateException e) {
            txtScreen.setText(e.getMessage());
        }
    }

    public void onClear(View view) {
        expressionBuilder.clear();
        txtScreen.setText(expressionBuilder.toString());
    }

    public void onRemoveLast(View view) {
        if (expressionBuilder.size() > 0) {
            expressionBuilder.removeLast();
        }
        txtScreen.setText(expressionBuilder.toString());
    }

    public void onAppendCommand(View view) {
        expressionBuilder.add(btnCommands.get(view.getId()));
        txtScreen.setText(expressionBuilder.toString());
    }

    private void fillCommandTyples(Map<Integer, SimpleExpressionCommand> matrix) {
        matrix.put(R.id.btnZero,  SimpleExpressionCommand.ZERO);
        matrix.put(R.id.btnOne,   SimpleExpressionCommand.ONE);
        matrix.put(R.id.btnTwo,   SimpleExpressionCommand.TWO);
        matrix.put(R.id.btnThree, SimpleExpressionCommand.THREE);
        matrix.put(R.id.btnFour,  SimpleExpressionCommand.FOUR);
        matrix.put(R.id.btnFive,  SimpleExpressionCommand.FIVE);
        matrix.put(R.id.btnSix,   SimpleExpressionCommand.SIX);
        matrix.put(R.id.btnSeven, SimpleExpressionCommand.SEVEN);
        matrix.put(R.id.btnEight, SimpleExpressionCommand.EIGHT);
        matrix.put(R.id.btnNine,  SimpleExpressionCommand.NINE);
        matrix.put(R.id.btnDot,   SimpleExpressionCommand.DOT);
        matrix.put(R.id.btnAdd,   SimpleExpressionCommand.ADD);
        matrix.put(R.id.btnSubstract, SimpleExpressionCommand.SUBSTRACT);
        matrix.put(R.id.btnMultiply,  SimpleExpressionCommand.MULTIPLY);
        matrix.put(R.id.btnDivide,    SimpleExpressionCommand.DIVIDE);
        matrix.put(R.id.btnParenthesis, SimpleExpressionCommand.PARENTHESIS);
    }
}
