package com.drunkpacifist.androidcalculator.engine.exceptions;

/**
 * Exception during evaluation of postfix expression
 */
public class EvaluateException extends Exception {

    /**
     * Creates instance of the exception
     *
     * @param e parent exception
     */
    public EvaluateException(Exception e) {
        super(e);
    }

    /**
     * Creates instance of the exception
     *
     * @param message error message
     */
    public EvaluateException(String message) {
        super(message);
    }
}
