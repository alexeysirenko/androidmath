package com.drunkpacifist.androidcalculator.engine.tokens.numbers;

import com.drunkpacifist.androidcalculator.engine.tokens.Token;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Token to store numeric values, immutable
 */
public class NumericToken extends Token {

    /**
     * Useful zero constant.
     */
    public static final NumericToken ZERO = NumericToken.valueOf(0);

    /** Token value */
    private final double value;

    /**
     * Used for correct formatting of double values.
     */
    private static DecimalFormat numberFormatter = new DecimalFormat("#", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    /**
     * Executed from static context, formatter adjustment.
     */
    static {
        numberFormatter.setMaximumFractionDigits(340); // DecimalFormat.DOUBLE_FRACTION_DIGITS
    }

    /**
     * Creates an instance of class
     *
     * @param value number value
     */
    private NumericToken(double value) {
        this.value = value;
    }

    /**
     * Creates an instance of class using provided number string
     *
     * @param value number value
     * @return instance of created token, not null
     * @throws NumberFormatException in case of string does not contain numeric value
     */
    public static NumericToken valueOf(String value) throws NumberFormatException {
        return valueOf(Double.parseDouble(value));
    }

    /**
     * Creates an instance of class using provided number
     *
     * @param value umber value
     * @return instance of created token, not null
     */
    public static NumericToken valueOf(double value) {
        return new NumericToken(value);
    }

    /**
     * Token value getter
     *
     * @return token value
     */
    public double getValue() {
        return value;
    }

    /**
     * Check is provided string a part of a number
     *
     * @param operator checked string, not null
     * @return result
     */

    public static boolean isNumber(String operator) {
        return operator.matches("^[0-9\\.]+?$");
    }

    /**
     * This returns a String representation of the NumericToken
     *
     * @return string presentation
     */
    @Override
    public String toString() {
        return numberFormatter.format(value);
    }
}
