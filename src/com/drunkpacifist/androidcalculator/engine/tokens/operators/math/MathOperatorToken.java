package com.drunkpacifist.androidcalculator.engine.tokens.operators.math;

import com.drunkpacifist.androidcalculator.engine.tokens.operators.OperatorToken;
import com.drunkpacifist.androidcalculator.engine.tokens.operators.math.executor.MathOperator;

/**
 * Token to store math operators, immutable
 */
public class MathOperatorToken extends OperatorToken {

    /**
     * Pre-defined plus operator constant.
     */
    public static final MathOperatorToken PLUS = MathOperatorToken.valueOf(MathOperator.PLUS.toString());

    /**
     * Pre-defined minus operator constant.
     */
    public static final MathOperatorToken MINUS = MathOperatorToken.valueOf(MathOperator.MINUS.toString());

    /**
     * Pre-defined multiply operator constant.
     */
    public static final MathOperatorToken MULTIPLY = MathOperatorToken.valueOf(MathOperator.MULTIPLY.toString());

    /**
     * Pre-defined divide operator constant.
     */
    public static final MathOperatorToken DIVIDE = MathOperatorToken.valueOf(MathOperator.DIVIDE.toString());

    /**
     * Pre-defined power operator constant.
     */
    public static final MathOperatorToken POWER = MathOperatorToken.valueOf(MathOperator.POWER.toString());

    /**
     * Pre-defined unary minus operator constant.
     */
    public static final MathOperatorToken UNARYMINUS = MathOperatorToken.valueOf(MathOperator.UNARYMINUS.toString());

    /** Current math operator */
    private final MathOperator operator;

    /**
     * Creates class instance
     *
     * @param executor required MathOperator to wrap around
     */
    private MathOperatorToken(MathOperator executor) {
        this.operator = executor;
    }

    /**
     * Creates class instance using operator name string
     *
     * @param operatorName operator string, not null
     * @return new MathOperatorToken instance, not null
     * @throws IllegalArgumentException in case if operator name is unknown
     */
    public static MathOperatorToken valueOf(String operatorName) throws IllegalArgumentException {
        return new MathOperatorToken(MathOperator.byOperatorName(operatorName));
    }

    /**
     * Check is provided operator name belong to any known operators
     *
     * @param operatorName name of operator
     * @return is provided operator name belong to any known operators
     */
    public static boolean isOperatorSupported(String operatorName) {
        for (MathOperator executor: MathOperator.values()) {
            if (executor.toString().equals(operatorName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Perform calculation of provided operands
     *
     * @param args provided operands
     * @return calculated value
     * @throws ArithmeticException in case of arithmetic exception occurred
     */
    public double calculate(double[] args) throws ArithmeticException {
        return operator.calculate(args);
    }

    /**
     * This returns a String representation of the MathOperatorToken
     *
     * @return string presentation
     */
    public String toString() {
        return operator.toString();
    }

    /**
     * This returns priority of current operator
     *
     * @return operator priority
     */
    public int getPriority() {
        return operator.getPriority();
    }

    /**
     * This returns a number of operands required for current operator
     *
     * @return count
     */
    public int getOperandsCount() {
        return operator.getOperandsCount();
    }
}