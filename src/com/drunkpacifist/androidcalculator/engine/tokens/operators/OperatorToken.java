package com.drunkpacifist.androidcalculator.engine.tokens.operators;


import com.drunkpacifist.androidcalculator.engine.tokens.Token;

/**
 * Token to store operators such as brackets and mathematical operators (for inheritance). Immutable
 */
public abstract class OperatorToken extends Token implements Comparable<OperatorToken> {

    /**
     * This returns priority of current operator
     *
     * @return operator priority
     */
    public abstract int getPriority();

    /**
     * This compares priority of two operators
     *
     * @param operatorToken operator to compare, not null
     * @return comparison result
     */
    @Override
    public int compareTo(OperatorToken operatorToken) {
        if (this.getPriority() == operatorToken.getPriority()) {
            return 0;
        } else {
            return (this.getPriority() > operatorToken.getPriority()) ? 1 : -1;
        }
    }
}