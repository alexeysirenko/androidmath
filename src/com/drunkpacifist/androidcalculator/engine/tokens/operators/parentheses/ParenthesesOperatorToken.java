package com.drunkpacifist.androidcalculator.engine.tokens.operators.parentheses;

import com.drunkpacifist.androidcalculator.engine.tokens.operators.OperatorToken;
import com.drunkpacifist.androidcalculator.engine.tokens.operators.parentheses.executor.ParenthesesOperator;

/**
 * Token to store parentheses operators, immutable
 */
public class ParenthesesOperatorToken extends OperatorToken {

    /**
     * Useful pre-defined open parenthesis constant
     */
    public static final ParenthesesOperatorToken OPEN = ParenthesesOperatorToken.valueOf(ParenthesesOperator.OPEN.toString());

    /**
     * Useful pre-defined close parenthesis constant
     */
    public static final ParenthesesOperatorToken CLOSE = ParenthesesOperatorToken.valueOf(ParenthesesOperator.CLOSE.toString());

    /** Current parentheses */
    private final ParenthesesOperator operator;

    /**
     * Creates class instance
     *
     * @param operator required ParenthesesOperator to wrap around
     */
    private ParenthesesOperatorToken(ParenthesesOperator operator) {
        this.operator = operator;
    }

    /**
     * Creates class instance using operator name string
     *
     * @param operatorName operator string, not null
     * @return new ParenthesesOperatorToken instance, not null
     * @throws IllegalArgumentException in case if operator name is unknown
     */
    public static ParenthesesOperatorToken valueOf(String operatorName) throws IllegalArgumentException {
        return new ParenthesesOperatorToken(ParenthesesOperator.byOperatorName(operatorName));
    }

    /**
     * Check is provided operator name belong to any known operators
     *
     * @param operatorName name of operator
     * @return is provided operator name belong to any known operators
     */
    public static boolean isOperatorSupported(String operatorName) {
        for (ParenthesesOperator executor: ParenthesesOperator.values()) {
            if (executor.toString().equals(operatorName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This returns priority of current operator
     *
     * @return operator priority
     */
    @Override
    public int getPriority() {
        return operator.getPriority();
    }

    /**
     * This checks for opening parentheses
     *
     * @return is bracket opening
     */
    public boolean isOpen() {
        return operator.isOpen();
    }

    /**
     * This checks for closing parentheses
     *
     * @return is bracket closing
     */
    public boolean isClose() {
        return !operator.isOpen();
    }

    /**
     * Returns string representation of object
     *
     * @return string representation of object
     */
    public String toString() {
        return operator.toString();
    }

}
