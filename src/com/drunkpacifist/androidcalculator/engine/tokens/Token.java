package com.drunkpacifist.androidcalculator.engine.tokens;

/**
 * Basic class to store math expression elements (for inheritance). Immutable
 */
public abstract class Token {}
