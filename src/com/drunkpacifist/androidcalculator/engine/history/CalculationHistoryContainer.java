package com.drunkpacifist.androidcalculator.engine.history;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Calculation history container
 */
public class CalculationHistoryContainer implements IHistory {

    /** History holder */
    private final List<String> history = new ArrayList<>();

    /**
     * Add a line to the end of history list
     *
     * @param item a string to remember, not null
     */
    public void add(final String item) {
        history.add(item);
    }

    /**
     * Return specific number of last items or less if history does contains that many.
     *
     * @param count number of items, more or equals to 0
     * @return history list, empty in case of no history written, not null
     */
    public List<String> getLast(final int count) {
        int availableCount = Math.min(count, history.size());
        return history.subList(history.size() - availableCount, availableCount);
    }

    /**
     * Return entire history list
     *
     * @return history history, not null
     */
    public List<String> getAll() {
        return new ArrayList<>(history);
    }

    /**
     * Return history list of unique expressions
     *
     * @return history history, not null
     */
    public List<String> getUnique() {
        return new ArrayList<>(new LinkedHashSet<>(history));
    }
}
