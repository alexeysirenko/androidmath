package com.drunkpacifist.androidcalculator.engine.history;

import java.util.List;

/**
 * Wonderful decorator around calculation history container with result validation and
 * nicer interface
 */
public class CalculationHistoryDecorator implements IHistory {

    /** History container */
    private final IHistory historyContainer;

    /**
     * Creatse class instance
     *
     * @param historyContainer
     */
    public CalculationHistoryDecorator(IHistory historyContainer) {
        this.historyContainer = historyContainer;
    }

    /**
     * Add a line to the end of history list
     *
     * @param item a string to remember, not null
     */
    public void add(final String item) {
        historyContainer.add(item);
    }

    /**
     * Return the last item of history list
     * <p>
     * In case of empty history will return one-item list
     * with empty history message, otherwise will add a string about
     * the last expression to the top of the list
     *
     * @return list of message strings, not null
     */
    public List<String> getLast(final int count) {
        List<String> result = historyContainer.getLast(count);
        result.add(0, (result.size() > 0 ? String.format("last %d items:", result.size()) : "history is empty"));
        return result;
    }

    /**
     * Return entire history list
     * <p>
     * In case of empty history will return one-item list
     * with empty history message, otherwise will add a string
     * that it is an entire history to the top of the list
     *
     * @return history list, not null
     */
    public List<String> getAll() {
        List<String> result = historyContainer.getAll();
        result.add(0, (result.size() > 0 ? String.format("history (%d items):", result.size()) : "history is empty"));
        return result;
    }

    /**
     * Return unique history list
     * <p>
     * In case of empty history will return one-item list
     * with empty history message, otherwise will add a string
     * that it is an unique history to the top of the list
     *
     * @return history list, not null
     */
    public List<String> getUnique() {
        List<String> result = historyContainer.getUnique();
        result.add(0, (result.size() > 0 ? String.format("unique history (%d items):", result.size()) : "unique history is empty"));
        return result;
    }
}
