package com.drunkpacifist.androidcalculator.engine.tokenprocessors.sorter;

import com.drunkpacifist.androidcalculator.engine.exceptions.PostfixConvertException;
import com.drunkpacifist.androidcalculator.engine.tokens.numbers.NumericToken;
import com.drunkpacifist.androidcalculator.engine.tokens.operators.OperatorToken;
import com.drunkpacifist.androidcalculator.engine.tokens.operators.parentheses.ParenthesesOperatorToken;
import com.drunkpacifist.androidcalculator.engine.tokens.Token;

import java.util.*;

/**
 * Converts math expression from infix to postfix form
 */
public class TokenListPostfixConverter {

    /**
     * Convert provided infix expression to postfix form
     *
     * @param infixTokens expression as list of previously parsed tokens
     * @return list of tokens, not null
     * @throws PostfixConvertException in case of invalid expression
     */
    public List<Token> convertInfixToPostfix(List<Token> infixTokens) throws PostfixConvertException {
        List<Token> postfixTokens = new ArrayList<>();
        Deque<OperatorToken> operatorStack = new ArrayDeque<>();
        for (Token token : infixTokens) {
            if (isNumber(token)) {
                postfixTokens.add(token);
            } else if (isParentheses(token)) {
                ParenthesesOperatorToken parenthesis = (ParenthesesOperatorToken) token;
                if (parenthesis.isOpen()) {
                    operatorStack.push(parenthesis);
                } else if (parenthesis.isClose()) {
                    // If close parentheses found enumerate math operators previously pushed to stack
                    // and add them to result list
                    try {
                        OperatorToken topOperatorToken = operatorStack.pop();
                        while (true) {
                            if ((isParentheses(topOperatorToken)) &&
                                    ((ParenthesesOperatorToken) topOperatorToken).isOpen()) {
                                break;
                            }
                            postfixTokens.add(topOperatorToken);
                            topOperatorToken = operatorStack.pop();
                        }
                    } catch (NoSuchElementException e) {
                        throw new PostfixConvertException("Unpaired parentheses");
                    }
                }
            } else if (isOperator(token)) {
                OperatorToken operatorToken = (OperatorToken) token;
                // Sort math operators by priority
                while (!operatorStack.isEmpty() && (operatorStack.peek().compareTo(operatorToken) >= 0)) {
                    postfixTokens.add(operatorStack.pop());
                }
                operatorStack.push(operatorToken);
            } else {
                throw new AssertionError(String.format("Unexpected token '%s'", token));
            }
        }
        // Add operators left in stack to result list
        while (!operatorStack.isEmpty()) {
            OperatorToken operatorToken = operatorStack.pop();
            if (isParentheses(operatorToken)) {
                throw new PostfixConvertException("Unpaired parentheses");
            }
            postfixTokens.add(operatorToken);
        }
        return postfixTokens;
    }

    /**
     * Check is provided token a number
     *
     * @param token checked token
     * @return result
     */
    private boolean isNumber(Token token) {
        return token instanceof NumericToken;
    }

    /**
     * Check is provided token an math operator
     *
     * @param token checked token
     * @return result
     */
    private boolean isOperator(Token token) {
        return token instanceof OperatorToken;
    }

    /**
     * Check is provided token a parentheses
     *
     * @param token checked token
     * @return result
     */
    private boolean isParentheses(Token token) {
        return token instanceof ParenthesesOperatorToken;
    }
}
