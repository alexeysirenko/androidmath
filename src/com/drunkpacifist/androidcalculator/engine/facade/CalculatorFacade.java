package com.drunkpacifist.androidcalculator.engine.facade;


import com.drunkpacifist.androidcalculator.engine.exceptions.EvaluateException;
import com.drunkpacifist.androidcalculator.engine.exceptions.PostfixConvertException;
import com.drunkpacifist.androidcalculator.engine.history.CalculationHistoryContainer;
import com.drunkpacifist.androidcalculator.engine.history.CalculationHistoryDecorator;
import com.drunkpacifist.androidcalculator.engine.history.IHistory;
import com.drunkpacifist.androidcalculator.engine.service.CalculatorService;
import com.drunkpacifist.androidcalculator.engine.tokenprocessors.evaluator.TokenListEvaluator;
import com.drunkpacifist.androidcalculator.engine.tokenprocessors.sorter.TokenListPostfixConverter;
import com.drunkpacifist.androidcalculator.engine.tokens.Token;

import java.util.List;

/**
 * Main facade class to provide simplified interface to the calculator service
 */
public class CalculatorFacade {

    /**  Calculator service */
    private final CalculatorService service;

    /**
     * Creates an instance of the facade class
     */
    public CalculatorFacade() {
        TokenListEvaluator evaluator = new TokenListEvaluator();
        TokenListPostfixConverter converter = new TokenListPostfixConverter();
        IHistory history = new CalculationHistoryDecorator(new CalculationHistoryContainer());
        this.service = new CalculatorService(converter, evaluator, history);
    }

    /**
     * Process string expression.
     * <p>
     * In case of error will return an error message string.
     * For more information about acceptable commands and expression format see
     * @see com.sysgears.simplecalculator.service.CalculatorService
     *
     * @param expression math expression or program command, not null
     * @return list of result strings, not null
     */
    public double processExpression(List<Token> tokens) throws PostfixConvertException, EvaluateException {
        return service.calculate(tokens);
    }
}
