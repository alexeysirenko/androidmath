package com.drunkpacifist.androidcalculator.engine.service;

import com.drunkpacifist.androidcalculator.engine.exceptions.EvaluateException;
import com.drunkpacifist.androidcalculator.engine.exceptions.PostfixConvertException;
import com.drunkpacifist.androidcalculator.engine.history.IHistory;
import com.drunkpacifist.androidcalculator.engine.tokenprocessors.evaluator.TokenListEvaluator;
import com.drunkpacifist.androidcalculator.engine.tokenprocessors.sorter.TokenListPostfixConverter;
import com.drunkpacifist.androidcalculator.engine.tokens.Token;

import java.util.List;

/**
 * Basic calculation service class.
 * <p>
 * Provides step-by-step calculation of the math expression.
 */
public class CalculatorService {

    /** Converter of parsed expression to postfix form */
    private final TokenListPostfixConverter converter;

    /** Final evaluator of postfix expression */
    private final TokenListEvaluator evaluator;

    /** Calculation history holder */
    private final IHistory history;

    /**
     * Creates an instance of service class
     *
     * @param converter expression infix-to-postfix converter
     * @param evaluator postfix expression evaluator
     * @param history calculation history holder
     */
    public CalculatorService(TokenListPostfixConverter converter,
                             TokenListEvaluator evaluator, IHistory history) {
        this.converter = converter;
        this.evaluator = evaluator;
        this.history = history;
    }

    /**
     * Calculate math expression step-by-step and save it to the history
     *
     * @param tokens math expression presented as list of tokens in normal infix form, not null
     * @return list of result strings, not null
     */
    public double calculate(List<Token> tokens) throws PostfixConvertException, EvaluateException {
        tokens = converter.convertInfixToPostfix(tokens);
        double result = evaluator.evaluate(tokens);
        history.add(String.format("%s = %s", tokens, result));
        return result;
    }
}
